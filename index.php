<!DOCTYPE html><?php $homePage = 1;?>
<html>
<head>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<title>Kelvin Batiste</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style>
		#cycler{position:relative;}
		#cycler img{position:absolute;z-index:1}
		#cycler img.active{z-index:3}
		#kelvinName{display:none;}
	</style>
	<script>
	function cycleImages(){
	      var $active = $('#cycler .active');
	      var $next = ($active.next().length > 0) ? $active.next() : $('#cycler img:first');
	      $next.css('z-index',2);//move the next image up the pile
	      $active.fadeOut(1500,function(){//fade out the top image
		  $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
	          $next.css('z-index',3).addClass('active');//make the next image the top one
	      });
	}

	$(document).ready(function(){
			var eventFired = 0;
			if ($(window).width() < 768) {
				$( "#homeImage" ).attr({src: "images/homeImagemobile.png"});
				$( "#homeImage2" ).attr({src: "images/homeImageKBmobile.png"});
				$( "#creativedirector" ).attr({src: "images/creative-directormobile.png"});
			}
			else {
				$( "#homeImage" ).attr({src: "images/homeImage.png"});
				$( "#homeImage2" ).attr({src: "images/homeImageKB.png"});
				$( "#creativedirector" ).attr({src: "images/creative-director.png"});
			}

			$(window).on('resize', function() {
			    if (!eventFired) {
			        if ($(window).width() < 768) {
						$( "#homeImage" ).attr({src: "images/homeImagemobile.png"});
						$( "#homeImage2" ).attr({src: "images/homeImageKBmobile.png"});
						$( "#creativedirector" ).attr({src: "images/creative-directormobile.png"});
			        } else {
			            $( "#homeImage" ).attr({src: "images/homeImage.png"});
						$( "#homeImage2" ).attr({src: "images/homeImageKB.png"});
						$( "#creativedirector" ).attr({src: "images/creative-director.png"});
					}
				}
			});
			
			
	setTimeout(function(){
	cycleImages();
	}, 1000);
	setTimeout(function(){$( "#creativedirector" ).animate( 
		{opacity: 1},1500);}, 2000);
	setTimeout(function(){$( "#desktopNav ul li,button.navbar-toggle" ).animate( 
		{opacity: 1},1500);}, 3000);
	});
	</script>
</head>
<body>
	<?php include('include/nav.php');?>
	<div id="mainContainer" class="marginCenter">
		<div id="cycler" class="marginCenter">
			<img id="homeImage" class="marginCenter active" src="" />
			<img id="homeImage2" class="marginCenter" src="" />
		</div>
		<img id="creativedirector"  src="" />
	</div>
</body>
</html>