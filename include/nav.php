<?php
if(!isset($homePage)){
$srcPrefix = "../";}
else $srcPrefix = "";
?><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-28403439-10', 'auto');
  ga('send', 'pageview');

</script>
	<nav id="mobileNav" class="visible-xs navbar navbar-default" style="width:100%;">
	        <div class="container-fluid">
	          <div class="navbar-header">
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	              <span class="sr-only">Toggle navigation</span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
				<?php if(!isset($homePage)){?>
					<a class="navbar-brand" href="<?=$srcPrefix?>"><img src="../images/kelvinbatiste.png"></a>
				<? }?>
	          </div>
	          <div id="navbar" class="navbar-collapse collapse">
	            <ul class="nav navbar-nav">
	              <li><a href="<?=$srcPrefix?>bio/">BIO</a></li><!--class="active"-->
	              <li><a href="<?=$srcPrefix?>portfolio/">PORTFOLIO</a></li>
	              <li><a href="<?=$srcPrefix?>contact/">CONTACT</a></li>
	              <li><a href="<?=$srcPrefix?>index/">INDEX</a></li>
	              <li><a href="<?=$srcPrefix?>events/">EVENTS</a></li>
	            </ul>
	          </div><!--/.nav-collapse -->
	        </div><!--/.container-fluid -->
	      </nav>
	<nav id="desktopNav" class="hidden-xs marginCenter">
		<?php if(!isset($homePage)){?>
		<a href="<?=$srcPrefix?>"><img id="kelvinName" src="../images/kelvinbatiste.png"></a>
		<? }?>
		<ul>
			<a href="<?=$srcPrefix?>bio/"><li>BIO</li></a>
			<a href="<?=$srcPrefix?>portfolio/"><li>PORTFOLIO</li></a>
			<a href="<?=$srcPrefix?>contact/"><li>CONTACT</li></a>
			<a href="<?=$srcPrefix?>index/"><li>INDEX</li></a>
			<a href="<?=$srcPrefix?>events/"><li>EVENTS</li></a>
		</ul>
	</nav>