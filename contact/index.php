<!DOCTYPE html>
<html>
<head>
	<?php # Include the Autoloader (see "Libraries" for install instructions)
//	require 'mailgun-php/vendor/autoload.php';
//	use Mailgun\Mailgun;
	# Instantiate the client.
//	$mgClient = new Mailgun('key-2457caa5969c9dadf2dc3a31eed2a733');
//	$domain = "mail.kelvinbatiste.com";

	?>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
	<title>Kelvin Batiste - Contact</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<style>
	#desktopNav ul li{opacity: 1;}
	button.navbar-toggle{opacity:1;}
	.contact{font-family: 'Amiri', serif;font-size:18px;}
	#social{margin-right: auto;
    margin-left: auto;
    color: #d3d3d3;
    width: 176px;}
	#social a img {
	    width: 55px;
	}
	
	
	</style>
	<script>

	$(document).ready(function(){
		 var    originalImageSrc1 = "../images/1459447502_instagram_online_social_media_photo.png";
		var newImageSrc1 = "../images/1459455165_instagram_online_social_media.png";
		var id = "#ig";
		  $(id)
		        .on('mouseover', function() {$(id).attr('src', newImageSrc1);
		        }).on('mouseout', function() {$(id).attr('src', originalImageSrc1);});

		 var    originalImageSrc2 = "../images/1459447514_online_social_media_tumblr.png";
		var newImageSrc2 = "../images/1459455172_tumblr_online_social_media.png";
		var id2 = "#tumblr";
		  $(id2)
		        .on('mouseover', function() {$(id2).attr('src', newImageSrc2);
		        }).on('mouseout', function() {$(id2).attr('src', originalImageSrc2);});

		 var    originalImageSrc3 = "../images/1459447507_mail_email_envelope_send_message.png";
		var newImageSrc3 = "../images/1459455170_email_mail_envelope_send_message.png";
		var id3 = "#email";
		  $(id3)
		        .on('mouseover', function() {$(id3).attr('src', newImageSrc3);
		        }).on('mouseout', function() {$(id3).attr('src', originalImageSrc3);});

	});
	</script>
</head>
<body>
	<?php include('../include/nav.php');?>
	<div id="mainContainer" class="marginCenter contact">
		<?php
		
		# Make the call to the client.
//		$result = $mgClient->sendMessage($domain, array(
//		    'from'    => 'Friendly Technologies <noreply@friendlytechnologiesllc.com>',
//		    'to'      => $email,
//		    'subject' => "Requested Information from Architectural Tile Designs",
//		    'text'    => "Your temporary password is: ".$random . "\n\nIt will expire on ".$tomorrow . "\n\nhttp://friendlytechnologiesllc.com/tobysite/login.php"
//		));
		
		
		?>
		<br /><br />
		
		<br /><br /><br /><br />
		<div id="social">
			<a href="https://instagram.com/filthykel/" target="_blank"><img id="ig" src="../images/1459447502_instagram_online_social_media_photo.png"></a>
			<a href="http://filthykel.tumblr.com/" target="_blank"><img  id="tumblr" src="../images/1459447514_online_social_media_tumblr.png"></a>
			<a target="_blank" href="mailto:kelvin@kelvinbatiste.com"><img  id="email" src="../images/1459447507_mail_email_envelope_send_message.png"></a>
			</div>
	</div>
</body>
</html>

