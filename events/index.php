<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
	<title>Kelvin Batiste - Events</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/html5lightbox/html5lightbox.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	<link rel="stylesheet" href="../portfolio/prettyPhoto_compressed_3.1.6/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
		<script src="../portfolio/prettyPhoto_compressed_3.1.6/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<style>
	#desktopNav ul li{opacity: 1;}
	button.navbar-toggle{opacity:1;}
	#social{margin-right: auto;
    margin-left: auto;
    color: #d3d3d3;
    width: 40px;}
	#social a img {
	    width: 40px;
	}
	.eventsGallery{margin-right: auto;
    margin-left: auto;}
	.eventsGallery td img{width:100%;padding:5px;}
	
	
	.slick-slide img,.slick-slide a{width:100%;}
	</style>

	<script>
	$(document).ready(function(){

//		if ($(window).width() >= 768) { 
		    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools: false,show_title:false, theme:'dark_square',keyboard_shortcuts:false});
//		}
	  });


	$(document).ready(function(){
		 var    originalImageSrc2 = "../images/1459447514_online_social_media_tumblr.png";
		var newImageSrc2 = "../images/1459455172_tumblr_online_social_media.png";
		var id2 = "#tumblr";
		  $(id2)
		        .on('mouseover', function() {$(id2).attr('src', newImageSrc2);
		        }).on('mouseout', function() {$(id2).attr('src', originalImageSrc2);});

				$('.multiple-items').slick({
				  infinite: true,
				  slidesToShow: 5,
				  slidesToScroll: 1,
				 autoplay: true,
				  autoplaySpeed: 2000,
				prevArrow: false,
				nextArrow: false,
				infinite: true,
				responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 4,
				        slidesToScroll: 1
				        
				      }
				    },
				    {
				      breakpoint: 600,
				      settings: {
				        slidesToShow: 3,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    }
				    // You can unslick at a given breakpoint now by adding:
				    // settings: "unslick"
				    // instead of a settings object
				  ]
				});

	});
	</script>
	<link rel="stylesheet" type="text/css" href="../include/slick-1.6.0/slick/slick.css"/>


	<script type="text/javascript" src="../include/slick-1.6.0/slick/slick.min.js"></script>

	
</head>
<body>
	<?php include('../include/nav.php');?>
	<div class="eventpageMainContainer marginCenter events">
		<img class="marginCenter" src="image1.JPG"><br />
		<div style="font-family: 'Lato', sans-serif;text-align:right;margin-right:50px;font-size:10px;">B'Nite<br />
		A Beyonce Party<br />
		10.29.16
		</div><br />
	</div>
	<div style="width: 80%;" class="eventsGallery bnite">
		<div class="slider multiple-items">
			<div><a rel="prettyPhoto" href="bnite/File_000.jpg"><img src="bnite/File_000.jpg"></a></div>
			<div><a rel="prettyPhoto" href="bnite/File_001.jpg"><img src="bnite/File_001.jpg"></a></div>
			<div><a rel="prettyPhoto" href="bnite/File_002.jpg"><img src="bnite/File_002.jpg"></a></div>
			<div><a rel="prettyPhoto" href="bnite/File_003.jpg"><img src="bnite/File_003.jpg"></a></div>
			<div><a rel="prettyPhoto" href="bnite/File_004.jpg"><img src="bnite/File_004.jpg"></a></div>
			<div><a rel="prettyPhoto" href="bnite/File_005.jpg"><img src="bnite/File_005.jpg"></a></div>
		</div>
		<br />
		<div class="video_thumbs">
			<a href="bnite/File_006.mp4" controls="true" autoplay class="html5lightbox" data-width="480" data-height="320"><img class="video_thumb" src="bnite/File_006.png"></a>		
			<a href="bnite/File_007.mp4" class="html5lightbox" data-width="480" data-height="320"><img class="video_thumb" src="bnite/File_007.png"></a>		
			<a href="bnite/File_008.mp4" class="html5lightbox" data-width="480" data-height="320"><img class="video_thumb" src="bnite/File_008.png"></a>		
			<a href="bnite/File_009.mp4" class="html5lightbox" data-width="480" data-height="320"><img class="video_thumb" src="bnite/File_009.png"></a>		
		</div>
	</div>
	<div class="eventpageMainContainer marginCenter events">
		<br /><br />		
		<img class="marginCenter" src="../images/invite.png"><br />
		<div style="font-family: 'Lato', sans-serif;text-align:right;margin-right:50px;font-size:10px;">Invitation designed by D'Ara Nazaryan</div><br /><br /><br />
	</div>
	<div style="width: 80%;" class="eventsGallery">
		<div class="slider multiple-items">
			<div><a rel="prettyPhoto" href="eventsImages/events01.jpg"><img src="eventsImages/events01.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events02.jpg"><img src="eventsImages/events02.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events03.jpg"><img src="eventsImages/events03.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events04.jpg"><img src="eventsImages/events04.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events05.jpg"><img src="eventsImages/events05.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events06.jpg"><img src="eventsImages/events06.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events07.jpg"><img src="eventsImages/events07.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events08.jpg"><img src="eventsImages/events08.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events09.jpg"><img src="eventsImages/events09.jpg"></a></div>
			<div><a rel="prettyPhoto" href="eventsImages/events10.jpg"><img src="eventsImages/events10.jpg"></a></div>
		</div>
	</div>
<br />
	<div id="social">
		<a style="display:none;" href="http://www.filthychronicles.tumblr.com" target="_blank"><img id="tumblr" src="../images/1459447514_online_social_media_tumblr.png"></a>
	</div>
	<br />
</body>
</html>

