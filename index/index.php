<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<title>Kelvin Batiste - Index</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<style>
	#desktopNav ul li{opacity: 1;}
	button.navbar-toggle{opacity:1;}

	</style>
	<script>

	$(document).ready(function(){
	
	
	});
	</script>
	<script>
	  $(function() {
	    $( "#accordion" ).accordion({heightStyle: "content",collapsible: true,icons: null,active:false});
	  });
	  </script>
</head>
<body>
	<?php include('../include/nav.php');?>
	<div id="mainContainer" class="marginCenter index">
		
		
		
		
		
		
		
		<div id="accordion">
		  <h3 onclick="this.blur()">MODELS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.instagram.com/agorbacheeva">Alena Gorbachish</a><br />
		    	<a target="_blank" href="http://www.instagram.com/iloveaudreyj">Audrey J</a><br />
				<a target="_blank" href="http://www.instagram.com/channingamandaa">Channing Amanda</a><br />
				<a target="_blank" href="http://www.instagram.com/misscolewoods">Cole Woods</a><br />
				<a target="_blank" href="http://www.modelmayhem.com/portfolio/3334049/viewall">EJ Brotons</a><br />
				<a target="_blank" href="https://www.facebook.com/gromovevgheni">Evgheni Gromov</a><br />
				<a target="_blank" href="http://www.modelmayhem.com/2589098">Grey</a><br />
				<a target="_blank" href="http://instagram.com/loveimanb">IMAN</a><br />
				<a target="_blank" href="https://www.facebook.com/jae.broome?ref=br_rs">Jae Broome</a><br />
				<a target="_blank" href="http://westhaveninternational.com/los-angeles/">Jessica Li</a><br />
				<a target="_blank" href="http://www.instagram.com/jessicaminter">Jessica Minter</a><br />
				<a target="_blank" href="http://www.katerigabrielle.com/">Kateri Gabrielle</a><br />
				<a target="_blank" href="http://www.modelmayhem.com/3698748">Leslie Lopez</a><br />
				<a target="_blank" href="http://www.instagram.com/lauradewit">Laura Dewitt</a><br />
				<a target="_blank" href="http://www.dmtm-inc.com/#!manuel-/ww4zq">Manuel Sanchez</a><br />
				<a target="_blank" href="http://www.modelmayhem.com/2853586">Matt Jordan</a><br />
				<a target="_blank" href="http://www.modelmayhem.com/3469776">Meagan Mitchell</a><br />
				<a target="_blank" href="http://www.instagram.com/meicathemodel">Meica</a><br />
				<a target="_blank" href="http://brokendollmodels.com/paige-packard">Paige Packard</a><br />
				<a target="_blank" href="http://instagram.com/beccadingemans">Rebecca Dingemans</a><br />
				<a target="_blank" href="http://www.instagram.com/sarahtemp_">Sarah</a><br />
				<a target="_blank" href="http://www.instagram.com/sohniahmedofficial">Sohni Ahmed</a><br />
				<a target="_blank" href="http://www.instagram.com/staystaybae">Stasiya Maria</a><br />
				<a target="_blank" href="http://m.imdb.com/name/nm3828463/">Tarneisha Stimage</a><br />
				<a target="_blank" href="http://www.instagram.com/taylorsherak">Taylor Sherak</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">PHOTOGRAPHERS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.adamfranz.com/fashion/">Adam Franz</a><br />
				<a target="_blank" href="http://instagram.com/ejwerkz">EJ WERKZ</a><br />
				<a target="_blank" href="http://www.riquemb.com">Enrique Bautista</a><br />
				<a target="_blank" href="http://www.greenergrassphotography.com/Fashion">Lan Doan</a><br />
				<a target="_blank" href="http://www.marcogallico.com">Marco Gallico</a><br />
				<a target="_blank" href="http://www.olegbogdan.com/">Oleg Bogdan</a><br />
				<a target="_blank" href="http://www.peterdangphotography.com/">Peter Dang</a><br />
				<a target="_blank" href="http://photospherestudios.com/">Shimel Kemoa</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">MAKEUP ARTISTS</h3 onclick="this.blur()">
		  <div>
		    <p>
				<a target="_blank" href="http://www.carmelitacontreras.com/">Carmelita Contreras</a><br />
				<a target="_blank" href="http://www.instagram.com/christophermilesmakeup">Christopher Miles</a><br />
				<a target="_blank" href="http://www.danielamgrasso.com/">Daniela Grasso</a><br />
		    	<a target="_blank" href="https://m.facebook.com/profile.php?id=100004783067442&tsid=0.056118656415492296&source=typeahead">Essence King</a><br />
				<a target="_blank" href="http://www.juliamakeupstudio.com/">Julia Makeup Studio</a><br />
				<a target="_blank" href="https://twitter.com/marisaajackson">Marisa A. Jackson</a><br />
				<a target="_blank" href="https://www.marluasoria.com">Marlu Soria</a><br />
				<a target="_blank" href="http://www.marvastokes.com/">Marva Stokes</a><br />
				<a target="_blank" href="https://m.youtube.com/watch?v=qGCJin3IVjo&feature=youtu.be">Natalie Pena</a><br />
				<a target="_blank" href="https://instagram.com/natasharg">Natasha Gressing</a><br />
				<a target="_blank" href="http://www.makeupbyrosy.com/">Rosy Amaya</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">HAIR STYLISTS</h3 onclick="this.blur()">
		  <div>
		    <p>
	    		<a target="_blank" href="http://www.getfashionhair.com">Doneisha Simone</a><br />
	    		<a target="_blank" href="http://www.instagram.com/edwardstylesyou">Edward Asuncion</a><br />
				<a target="_blank" href="http://www.juliamakeupstudio.com/">Julia Makeup Studio</a><br />
				<a target="_blank" href="http://www.marvastokes.com/">Marva Stokes</a><br />
				<a target="_blank" href="https://hairmomwife.wordpress.com/">Myesha Howze</a><br />
				<a target="_blank" href="https://m.youtube.com/watch?v=qGCJin3IVjo&feature=youtu.be">Natalie Pena</a><br />
				<a target="_blank" href="https://m.facebook.com/pages/Sarahglams/910779402325979">SARAHGLAMS</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">VIDEOGRAPHERS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://kainoakilcher.com/">Kainoa Klicher</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">FILMMAKERS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.diegoferres.com/">Diego Ferrès Devotto</a><br />
		    </p>
		  </div>
		
		  <h3 onclick="this.blur()">RETOUCHERS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.facebook.com/vincenzo.vocale1">Vincenzo Vocale</a><br />
				<a target="_blank" href="http://www.bimestral.com/#!yael-gripich/cw1o">Yael Gripich</a><br />
				<a target="_blank" href="http://www.frame-usa.com/?page_id=95">Sam Pags</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">DESIGNERS</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.dnaz.tv">D'Ara Nazaryan</a><br />
				<a target="_blank" href="http://www.dolceaquaswimwear.com">Dolce Aqua Swimwear</a><br />
				<a target="_blank" href="http://www.furrociousfurr.com">Furrocious Furr</a><br />
				<a target="_blank" href="http://www.queenrussia.com/">Russia Paul</a><br />
				<a target="_blank" href="http://www.steffikjewelry.com">Steffi K. Jewelry</a><br />
		    </p>
		  </div>
		  <h3 onclick="this.blur()">WARDROBE STYLIST</h3 onclick="this.blur()">
		  <div>
		    <p>
		    	<a target="_blank" href="http://www.erinpriethcitti.com/">Erin Citti</a><br />
		    	<a target="_blank" href="https://m.facebook.com/erinannnie">Erin Tyson</a><br />
				<a target="_blank" href="https://poshmark.com/closet/vinustyling">Nichole Joubert</a><br />
				<a target="_blank" href="http://www.instagram.com/sikasenro">Sika Senro</a><br />
				<a target="_blank" href="https://www.instagram.com/Willard.paul/">Willard Paul</a><br />
				<a target="_blank" href="http://m.modelmayhem.com/2611207">Zee Brown</a><br />
		    </p>
		  </div>
		</div>
	</div>
</body>
</html>

