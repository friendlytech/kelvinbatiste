<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<?php
	$portfolio = array(
		array("../portfolioImages/hustler.jpg","Hustlers&apos; Luck","","<p></p>Filmmaker Diego Ferrès Devotto",1),
		array("../portfolioImages/port1.JPG","Blackout","","<p></p><div class='left'>Ad Campaign for Designer Queen Russia<br />Shot by Peter Dang<br />Hair Myesha Howze<br />Makeup Rosy Amaya<br />Retouching Vincenzo Vocale<br /></div><div class='right'>Models<br />EJ Brotons Paige Packard <div class='nonmobile'></div>Kateri Gabrielle <div class='mobileOnly'></div>AudreyJ <div class='nonmobile'></div>Jae Broome Taylor Sherak <div class='nonmobile'></div><div class='mobileOnly'></div>Leslie Lopez Stasiya Maria</div>",1),
		array("../portfolioImages/port2.JPG","Blackout","","",1),
		array("../portfolioImages/port2a.jpg","Blackout","","",1),
		array("../portfolioImages/port2b.jpg","Blackout","","",1),
		array("../portfolioImages/port2c.jpg","Blackout","","",1),
		array("../portfolioImages/port2d.jpg","Blackout","","",1),
		array("../portfolioImages/port2e.jpg","Blackout","","",1),
		array("../portfolioImages/port3.JPG","Style Lookbook I","","<p></p>Photographer EJWERKZ<br />Hair/Makeup Natalie Pena<br />Wardrobe Stylist Erin Citti<br />Model Channing Amanda",0),
		array("../portfolioImages/port4a.JPG","Style Lookbook I","","",0),
//		array("../portfolioImages/port5.JPG","Style Lookbook I","","Photographer EJWERKZ<br />Model Channing Amanda<br />Wardrobe Stylist Erin Citti<br />Makeup/Hair Natalie Pena",0),
	//	array("../portfolioImages/port6.JPG","","Model Leslie Lopez Makeup/Hair Natalie Pena",1),
	//	array("../portfolioImages/port7.PNG","","Model Damian Makeup/Hair Natalie Pena",1)
		array("../portfolioImages/port8.JPG","Style Lookbook II","","<p></p>Photographer Lan Doan<br />Hair SARAHGLAMS<br />Makeup Essence King<br />Wardrobe Stylist Erin Tyson<br />Model Leslie Lopez",1),
		array("../portfolioImages/port9.JPG","Style Lookbook II","","",1),
		array("../portfolioImages/port10.JPG","Style Lookbook II","","",1),
		array("../portfolioImages/port11.JPG","Style Lookbook II","","",1),
		array("../portfolioImages/port11a.JPG","Style Lookbook II","","",1),
				array("../portfolioImages/portlookbook3-1.JPG","Style Lookbook III","","<div class='lookbook3spacemobile'></div><div class='left'>Photographer Kelvin Batiste<br />Hair/Makeup SARAHGLAMS<br />Wardrobe Stylist Willard Paul</div><div class='right'>Models<br />Grey<br />Matt Jordan<br />Manuel Sanchez</div><div style='clear: both;text-align:center;'>Retoucher Sam Pags Yael Gripich</div>",1),
				array("../portfolioImages/portlookbook3-2.JPG","Style Lookbook III","","",1),
				array("../portfolioImages/portlookbook3-3.JPG","Style Lookbook III","","",1),
		array("../portfolioImages/port12.JPG","Westhaven International","","<p></p>Editorial for Westhaven International<br />Photographer Shimel Kemoa<br />Hair SARAHGLAMS<br />Makeup Marisa Jackson<br />Model Jessica Li",1),
//		array("../portfolioImages/port13.JPG","Editorial for Westhaven International","Editorial for Westhaven International","Photographer Shimel Kemoa<br />Hair SARAHGLAMS<br />Makeup Marisa Jackson<br />Model Jessica Li",1),
		array("../portfolioImages/port14.JPG","Calafia","","Editorial for Oleg Bogdan Photography<br />Wardrobe Vin. U Styling Studio<br />Head Stylist Nichole Joubert<br />Hair & Makeup Marva Stokes<br />Assistant Stylist Zee Brown<br />Models IMAN &amp; Tarneisha Stimage",1),
		array("../portfolioImages/port15.JPG","Calafia","","",1),
		array("../portfolioImages/port16.JPG","Calafia","","",1),
		array("../portfolioImages/port17.JPG","Calafia","","",1),
		array("../portfolioImages/port18.JPG","Calafia","","",1),
		array("../portfolioImages/port19.JPG","Maneland","","Photographer Oleg Bogdan<br />Hair/Makeup Julia Makeup Studio<br />Model Leslie Lopez<br />Accessories Steffi K. Jewelry",1),
		array("../portfolioImages/port20.JPG","Maneland","","",1),
		array("../portfolioImages/port21.JPG","Maneland","","",1),
		array("../portfolioImages/port22.JPG","Maneland","","",1),
		array("../portfolioImages/port23.JPG","Maneland","","",1),
		array("../portfolioImages/port24.JPG","Maneland","","",1),
		array("../portfolioImages/port25.JPG","Maneland","","",1),
		
		array("../portfolioImages/port26.JPG","Armada","","Campaign for Steffi K. Jewelry<br />Photographer Adam Franz<br />Hair/Makeup Carmelita Contreras<br />Wardrobe Sika Senro<br />Models Meagan Mitchell, Meica, Sarah",1),
		array("../portfolioImages/port27.JPG","Armada","","",1),
		array("../portfolioImages/port28.JPG","Armada","","",1),
		array("../portfolioImages/port29.JPG","Armada","","",1),
		
		
		array("../portfolioImages/port30.JPG","67","","Photographer Marco Gallico<br />Models Rebecca Dingemans,Laura Dewitt<br />Creative Director Kelvin Batiste <br />MUA Natasha Gressing<br />Hair Doneisha Simone<br />Stylist Nichole Joubert<br />1st Asst Stylist Leah Haraway<br />2nd Asst Stylist Sha Haraway<br />",1),
		array("../portfolioImages/port31.JPG","67","","",1),
		array("../portfolioImages/port32.JPG","67","","",1),
		array("../portfolioImages/port33.JPG","67","","",1),
		array("../portfolioImages/port34.JPG","67","","",1),
		array("../portfolioImages/port35.JPG","67","","",1),
		
		
		array("../portfolioImages/franz_a_queenrussia001.jpg","перемотка вперед","","Lookbook for Queen Russia Designs<br />Photographer Adam Franz<br />Model Taylor Sherak<br />Hair/Makeup Daniela Grasso<br />",1),
		array("../portfolioImages/franz_a_queenrussia006.jpg","перемотка вперед","","",1),
		array("../portfolioImages/franz_a_queenrussia009.jpg","перемотка вперед","","",1),		
		array("../portfolioImages/franz_a_queenrussia010.jpg","перемотка вперед","","",1),
		array("../portfolioImages/franz_a_queenrussia012.jpg","перемотка вперед","","",1),
		array("../portfolioImages/franz_a_queenrussia014.jpg","перемотка вперед","","",1),
		array("../portfolioImages/franz_a_queenrussia017.jpg","перемотка вперед","","",1),
		array("../portfolioImages/franz_a_queenrussia026.jpg","перемотка вперед","","",1),

		array("../portfolioImages/vanity/_DSC8421.jpg","Vanity","","<div class='lookbook3spacemobile'></div><div class='left'>Photographer Enrique Bautista<br />Hairstylist Edward Asuncion<br />Makeup Marlu Soria<br />Makeup Christopher Miles<br />Designer Russia Paul</div><div class='right'>Models<br />Cole Woods<br />Alena Gorbachish<br />Evgheni Gromov<br />Jessica Minter<br />Sohni Ahmed</div>",1),
		
		
		
		array("../portfolioImages/vanity/_DSC8628.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8567.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8334.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8615.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8524.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8137.jpg","Vanity","","",1),
		array("../portfolioImages/vanity/_DSC8398.jpg","Vanity","","",1)
		
	);



	
	
	?>
	<title>Kelvin Batiste - Portfolio</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" href="prettyPhoto_compressed_3.1.6/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="prettyPhoto_compressed_3.1.6/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	
	<style>
	.left{float:left;width: 250px;    margin-left: 72px;}
	.right{float:left;width: 195px;    margin-left: 20px;}
	#desktopNav ul li{opacity: 1;}
	button.navbar-toggle{opacity:1;}

	</style>
	<script>

	$(window).ready(function(){
		
			window.swiperArray = [];
		<?php
		$i = 0;
		foreach($portfolio as $image){ $i++;
//			echo "<div class=\"swiper-slide\"><div class='swiper-image' style=\"background:url('".$image [0];
//			echo "');background-size:contain;background-repeat:no-repeat;background-position: center; \"></div><div class=\"caption\"><span>";
 		echo "swiperArray['".$image [0]."']='image$i';\n";
		echo "window['image$i'] = new Object();\n";
		echo "window['image$i'].location = \"$image[0]\";\n";
		echo "window['image$i'].album = \"$image[1]\";\n";
		echo "window['image$i'].campaign = \"$image[2]\";\n";
		echo "window['image$i'].info = \"$image[3]\";\n";
		}
		?>


		//console.log(window['image2']);
	});//console.log(window['image2']);
	
	
	$(document).ready(function(){

	if ($(window).width() >= 768) { 
	    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools: false,show_title:false, theme:'dark_square',keyboard_shortcuts:false});
	}
  });

	</script>
	
	<link rel="stylesheet" href="swiper.min.css">
	<style>
	.swiper-button-next{background-color:transparent;background-image:url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M27%2C22L27%2C22L5%2C44l-2.1-2.1L22.8%2C22L2.9%2C2.1L5%2C0L27%2C22L27%2C22z'%20fill%3D'%23d3d3d3'%2F%3E%3C%2Fsvg%3E");}
	.swiper-button-prev{background-color:transparent;background-image:url("data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20viewBox%3D'0%200%2027%2044'%3E%3Cpath%20d%3D'M0%2C22L22%2C0l2.1%2C2.1L4.2%2C22l19.9%2C19.9L22%2C44L0%2C22L0%2C22L0%2C22z'%20fill%3D'%23d3d3d3'%2F%3E%3C%2Fsvg%3E");}

	 html, body {
	     position: relative;
	     height: 100%;
	 }
.swiper-button-next,
.swiper-button-prev{display:none;}


	 </style>
</head>
<body>
	<?php include('../include/nav.php');?>
	<div id="mainContainer" class="marginCenter portfolio">

		<div class="swiper-container">
	        <div class="swiper-wrapper">
									<?php
									
									 $lastImage = '';
								/*	foreach($portfolio as $image){ if($image [1] != $lastImage AND $lastImage != '') continue;
										echo "<div class=\"swiper-slide\">";
										echo '<a href="'.$image [0].'" rel="prettyPhoto">';
										echo "<div class='swiper-image' style=\"background:url('".$image [0];
										echo "');background-size:contain;background-repeat:no-repeat;background-position: center; \"></div>";
										echo "</a>";
										echo "<div class=\"caption\">";
										//echo "<span>";
										// echo $image [2];
										//echo "</span><br />";
										echo "<p>";
										 echo $image [3];
										echo "</p></div></div>";
									$lastImage = $image [1];	
									}*/
									echo '<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="margin-right: 30px;"><iframe src="https://player.vimeo.com/video/164370036?title=0&byline=0&portrait=0" width="600" height="338" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
									
									
									?>

	        </div>
	        <!-- Add Pagination -->
	        <div class="swiper-pagination"></div>
			<!-- Add Arrows -->
			        <div class="swiper-button-next"></div>
			        <div class="swiper-button-prev"></div>
	    </div>
		<div class="mobileThumbnailSpace"></div>
		<?php
		$i = 0;
		$lastImage = "";
		foreach($portfolio as $image){$i++;
			if($image [1] == $lastImage) continue;
			
			echo "<div class=\"thumbnail-container";
			if($image [1] != "Blackout" AND $image [1] != "Westhaven International" AND $image [1] != "Hustlers&apos; Luck" ) echo " horizontalThumb";
			echo "\" onclick='scrollToTop(\"".$image [1]."\");'><div class='thumbnail-image marginCenter' style=\"background:url('".$image [0];
			echo "');background-size:contain;background-repeat:no-repeat;background-position: center; \"></div><div class=\"marginCenter thumbnail-caption";
			
			echo "\"";
			if($image [1] != "Blackout" AND $image [1] != "Westhaven International"  AND $image [1] != "Hustlers&apos; Luck" ){ echo " style='margin-top:25px;' ";}
			echo "><span>";
			/*if($image [3])*/ echo $image [1];
			echo "</span><br /><p>";
//			/*if($image [3])*/ echo $image [2];
			echo "</p></div></div>";
			$lastImage = $image [1];
		}
		?>
	</div>
	
	
	<!-- Swiper JS -->
    <script src="swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
    var swiper = new Swiper('.swiper-container', {
        //pagination: '.swiper-pagination',
        //paginationClickable: true,
speed: 700,
		nextButton: '.swiper-button-next',
		        prevButton: '.swiper-button-prev',
		        spaceBetween: 30,
		 loop: true,
		keyboardControl: true
    });


	function scrollToTop(slide){
		if(slide == "Hustlers' Luck"){
			$(".swiper-button-next,.swiper-button-prev").css("display","none");
			$('html, body').animate({scrollTop : 0},800,function(){
				
				swiper.removeAllSlides();
				slides = '<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="margin-right: 30px;"><iframe src="https://player.vimeo.com/video/164370036?title=0&byline=0&portrait=0" width="600" height="338" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
				swiper.appendSlide(slides);

				if ($(window).width() >= 768) { 
				    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools: false,show_title:false, theme:'dark_square',keyboard_shortcuts:false});
				}
			});
		}
		else
		$('html, body').animate({scrollTop : 0},800,function(){
			if(slide == "Westhaven International") $(".swiper-button-next,.swiper-button-prev").css("display","none");
			else $(".swiper-button-next,.swiper-button-prev").css("display","block");
			
			//swiper.slideTo(slide);
			swiper.removeAllSlides();
			var i = 0;
			for(var index in swiperArray) {

				slides = '<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="margin-right: 30px;"><a href="'+window[swiperArray[index]].location+'" rel="prettyPhoto"><div class="swiper-image" style="background:url('+window[swiperArray[index]].location+');background-size:contain;background-repeat:no-repeat;background-position: center; "></div></a><div class="caption">';
				if(window[swiperArray[index]].campaign != "") {slides = slides + '<span>'+window[swiperArray[index]].campaign+'</span><br>';}
				slides = slides + '<p>'+window[swiperArray[index]].info+'</p></div></div>';
				
				if(window[swiperArray[index]].album == slide){swiper.appendSlide(slides);i++;} 
			//console.log(window['image'+i].album);
			//console.log(slide);
			}	console.log(i);
			if(i > 1){swiper.params.loop=true;$(".swiper-button-prev").removeClass('swiper-button-disabled');swiper.slideTo(1,100);}
			else{swiper.params.loop=false;}
			
			
			if ($(window).width() >= 768) { 
			    $("a[rel^='prettyPhoto']").prettyPhoto({social_tools: false,show_title:false, theme:'dark_square',keyboard_shortcuts:false});
			}
		});	
		
	}
			
    </script>
</body>
</html>

