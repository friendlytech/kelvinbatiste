<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8>
	<link href='https://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
	<title>Kelvin Batiste - Biography</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<style>
	#desktopNav ul li{opacity: 1;}
	button.navbar-toggle{opacity:1;}

	</style>
	<script>

	$(document).ready(function(){
	
	
	});
	</script>
</head>
<body>
	<?php include('../include/nav.php');?>
	<div id="mainContainer" class="marginCenter biography">
		<p>Kelvin Batiste is a freelance Creative Director with 2 years in art direction.</p>

		<p>Batiste is responsible for crafting the creative look and feel of a fashion brand through editorial and campaign advertisement. Everything from concept to execution, Kelvin oversees the process—proactively working with the client to ensure the company's integrity is represented organically.</p>  

		<p>Of Louisianan descent, Kelvin's philosophy is to connect fashion stories through cultural resonance. As a result, he has collaborated with some of the industry's top international photographers. His understanding of marketing, brand objectives, consumer needs, and effective communication inspire the inimitable work that he is recognized for.</p> 

		<p>Further inspirations derive from architecture, pop culture, art, and theatre.</p> 

		<p>Batiste is a proud product of Los Angeles, where he is currently located.</p>
	</div>
</body>
</html>

